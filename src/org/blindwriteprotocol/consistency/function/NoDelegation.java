/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blindwriteprotocol.consistency.function;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.derby.jdbc.ClientDriver;
import org.blindwriteprotocol.consistency.init.CreateTableAndSequence;

/**
 *
 * @author khairul.a
 */
public class NoDelegation {

    private final String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
    private int numberTransaction = 0;
    private int maxNumberThreadAllowed = 1;
    private ArrayList numberActiveThread = new ArrayList();
    private Driver d = null;
    private static int totalTime = -2;
    private static final ArrayList responseTime = new ArrayList();
    private static ArrayList HistoryIdList = new ArrayList();
    private static final Random rand = new Random();
    private static final String[] operator = {"-", "+"};

    public static void main(String[] args) throws SQLException, InterruptedException {
        CreateTableAndSequence.init(Double.parseDouble("1000"));
        NoDelegation obj = new NoDelegation();
        obj.maxNumberThreadAllowed = 100;//Integer.parseInt(args[0]);
        obj.numberTransaction = 1000;//Integer.parseInt(args[1]);
        obj.d = new ClientDriver();
        for (int i = 1; i <= 24; i++) {
            obj.runTesting(i);
        }
        double avg = totalTime / responseTime.size();
        double throughput = 60 * (1000 / avg);
        double sd = 0;
        long minTime = 10000000;
        long maxTime = 0;
        for (int i = 0; i < responseTime.size(); i++) {
            long diff = (long) responseTime.get(i);
            if (diff < minTime) {
                minTime = diff;
            }
            if (diff > maxTime) {
                maxTime = diff;
            }
            sd += Math.pow(diff - avg, 2);
        }
        sd = Math.sqrt(sd / 22);
        System.out.println("totalTime::" + totalTime + "::avg::" + avg + "::minTime::" + minTime + "::maxTime::" + maxTime + "::sd::" + sd + "::throughput::" + throughput);
    }

    public void runTesting(int testNo) throws SQLException, InterruptedException {
        String accountNumber = BWPConstant.FIRST_ACCOUNT_NUMBER;
        HistoryIdList = new ArrayList();
        numberActiveThread = new ArrayList();
        Date start = new Date();
        for (int i = 1; i <= numberTransaction; i++) {
            while (numberActiveThread.size() >= maxNumberThreadAllowed) {
                BWPConstant.sleep(10);
            }
            numberActiveThread.add(1);
            Double transactionAmount = Double.parseDouble(getRandom() + "100");
            runInThread(accountNumber, transactionAmount);
        }
        while (HistoryIdList.size() < numberTransaction) {
            BWPConstant.sleep(100);
        }
        Date end = new Date();
        long diff = end.getTime() - start.getTime();
        System.out.println("testNo::" + testNo + "::response time::" + diff);
        switch (totalTime) {
            case -2:
                totalTime = -1;
                break;
            case -1:
                totalTime = 0;
                break;
            default:
                totalTime += diff;
                responseTime.add(diff);
                break;
        }
        BWPConstant.sleep(10000);
    }

    private String getRandom() {
        int rdm = rand.nextInt(operator.length);
        return operator[rdm];
    }

    private void runInThread(String accountNumber, Double transactionAmount) {
        Runnable r = new Runnable() {
            public void run() {
                blindWriteOperation(accountNumber, transactionAmount);
            }
        };
        new Thread(r).start();
    }

    private void blindWriteOperation(String accountNumber, Double transactionAmount) {
        int historyId = getSequence(BWPConstant.HISTORY_ID_SEQUENCE + accountNumber);
        aggregation(historyId, accountNumber, transactionAmount);
        synchronized (this) {
            if (numberActiveThread.size() > 0) {
                numberActiveThread.remove(numberActiveThread.size() - 1);
            }
            HistoryIdList.add(historyId);
        }
    }

    private void aggregation(int historyId, String accountNumber, Double transactionAmount) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            Map<String, Object> balanceRow = getAccoutId(conn, pstmt, accountNumber);
            int accountId = (int) balanceRow.get("account_id");
            insertTxnTable(conn, pstmt, accountId, historyId, transactionAmount, BWPConstant.NOT_APPROVED);
            Double resultBalanceAmount = transact(conn, pstmt, historyId, accountId, transactionAmount, balanceRow);
            if ((resultBalanceAmount.compareTo(Double.parseDouble("0")) >= 0)) {
                int rowcount = updateBalanceTbl(conn, pstmt, accountId, historyId, resultBalanceAmount);
                //System.out.println("updateBalanceTbl::historyId::" + historyId + "::rowcount::" + rowcount);
            }
        } catch (SQLException ex) {
            System.out.println("aggregation::historyId::" + historyId + "::ex::" + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
            }
        }
    }

    private Double transact(Connection conn, PreparedStatement pstmt, int historyId, int accountId, Double transactionAmount, Map<String, Object> balanceRow) throws SQLException {
        int prevHistoryId = (int) balanceRow.get("history_id");
        List<Map<String, Object>> dataTransaction = new ArrayList<Map<String, Object>>();
        if (historyId - prevHistoryId > 1) {
            PreparedStatement pstmtGetSettledHistory = conn.prepareStatement("select history_id,balance_amount from " + BWPConstant.TRANSACTION_TABLE + " h where h.account_id=? and h.history_id<? and h.transaction_status!=" + BWPConstant.NOT_APPROVED + " order by history_id desc FETCH FIRST 100 ROWS ONLY");
            PreparedStatement pstmtGetHistories = conn.prepareStatement("select history_id,transaction_amount from " + BWPConstant.TRANSACTION_TABLE + " h where h.account_id=? and h.history_id>? and h.history_id<? order by history_id asc");
            boolean keep_loop = true;
            while (keep_loop) {
                BWPConstant.sleep(100);
                dataTransaction = getHistories(pstmtGetHistories, accountId, prevHistoryId, historyId);
                if (dataTransaction.size() == historyId - prevHistoryId - 1) {
                    keep_loop = false;
                } else {
                    List<Map<String, Object>> setteledDataTransaction = getSettledHistory(pstmtGetSettledHistory, accountId, historyId);
                    if (setteledDataTransaction.size() > 0) {
                        balanceRow = setteledDataTransaction.get(0);
                        prevHistoryId = (int) balanceRow.get("history_id");
                    }
                }
            }
            pstmtGetHistories.close();
            pstmtGetSettledHistory.close();
        }
        Double previousBalanceAmount = Double.parseDouble(balanceRow.get("balance_amount").toString());
        for (Map<String, Object> row : dataTransaction) {
            Double transaction_amount = Double.parseDouble(row.get("transaction_amount").toString());
            Double temp = Double.sum(previousBalanceAmount, transaction_amount);
            if (temp.compareTo(Double.parseDouble("0")) >= 0) {
                previousBalanceAmount = temp;
            }
        }

        Double resultBalanceAmount = Double.sum(previousBalanceAmount, transactionAmount);
        int transactionStatus = BWPConstant.REJECTED;
        Double balanceAmount = previousBalanceAmount;
        if ((resultBalanceAmount.compareTo(Double.parseDouble("0")) >= 0)) {
            transactionStatus = BWPConstant.APPROVED;
            balanceAmount = resultBalanceAmount;
        }

        updateTxnStatus(conn, pstmt, accountId, historyId, prevHistoryId, dataTransaction.size(), transactionStatus, previousBalanceAmount, balanceAmount);
        insertHistoryTable(conn, pstmt, accountId, historyId, prevHistoryId, dataTransaction.size(), transactionStatus, previousBalanceAmount, balanceAmount, transactionAmount);

        PreparedStatement pstmtGetMax = conn.prepareStatement("select max(b.history_id) max_ from " + BWPConstant.TRANSACTION_TABLE + " b where b.account_id=? and b.history_id>=?");
        int maxHistoryId = getMax(pstmtGetMax, accountId, historyId);
        if (maxHistoryId > historyId) {
            pstmtGetMax.close();
            return Double.parseDouble("-1");
        }

        PreparedStatement pstmtGetMin = conn.prepareStatement("select min(b.history_id) min_ from " + BWPConstant.TRANSACTION_TABLE + " b where b.account_id=? and b.history_id<? and b.transaction_status=" + BWPConstant.NOT_APPROVED);
        int minHistoryId = getMin(pstmtGetMin, accountId, historyId);
        pstmtGetMin.close();
        if (minHistoryId <= 0) {
            minHistoryId = historyId;
        } else {
            minHistoryId = minHistoryId - 1;
        }
        if (minHistoryId > 0) {
            deleteTxnTable(conn, pstmt, accountId, minHistoryId);
            //System.out.println("deleteTxnTable::historyId::" + historyId + "::prevHistoryId::" + prevHistoryId + "::dataTransaction.size()::" + dataTransaction.size() + "::maxHistoryId::" + maxHistoryId + "::minHistoryId::" + minHistoryId);
        }
        maxHistoryId = getMax(pstmtGetMax, accountId, historyId);
        pstmtGetMax.close();
        if (maxHistoryId > historyId) {
            return Double.parseDouble("-1");
        }
        return balanceAmount;
    }

    private Connection getConnection() {
        try {
            return d.connect(url, null);
        } catch (SQLException ex) {
            BWPConstant.sleep(100);
            return getConnection();
        }
    }

    private String preventSqlInjection(String SequenceName) {
        /*prevent sql injection*/
        SequenceName = SequenceName.replaceAll(" ", "");
        SequenceName = SequenceName.replaceAll(",", "");
        SequenceName = SequenceName.replaceAll(";", "");
        SequenceName = SequenceName.replaceAll("-", "");
        SequenceName = SequenceName.replaceAll("/", "");
        return SequenceName;
    }

    private int getSequence(String SequenceName) {
        int seq_id = -1;
        Connection conn = null;
        try {
            conn = getConnection();
            SequenceName = preventSqlInjection(SequenceName);
            String selectSQL = "select NEXT VALUE FOR " + SequenceName + " as seq_id from SYSIBM.SYSDUMMY1";
            PreparedStatement pstmt = conn.prepareStatement(selectSQL);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                seq_id = rs.getInt("seq_id");
            }
            conn.close();
        } catch (SQLException ex) {
        }
        return seq_id;
    }

    private Map<String, Object> getAccoutId(Connection conn, PreparedStatement pstmt, String accountNumber) throws SQLException {
        pstmt = conn.prepareStatement("select account_id,balance_amount,history_id from " + BWPConstant.BALANCE_TABLE + " where account_number=?");
        pstmt.setString(1, accountNumber);
        return resultSetToMap(pstmt);
    }

    private Map<String, Object> getAccoutId(PreparedStatement pstmt, int accountId) throws SQLException {
        pstmt.setInt(1, accountId);
        return resultSetToMap(pstmt);
    }

    private void insertTxnTable(Connection conn, PreparedStatement pstmt, int accountId, int historyId, Double transactionAmount, int transaction_status) throws SQLException {
        pstmt = conn.prepareStatement("blind insert into " + BWPConstant.TRANSACTION_TABLE + "(history_id, account_id,transaction_amount,transaction_date,last_updated_date,transaction_status) values (?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?)");
        pstmt.setInt(1, historyId);
        pstmt.setInt(2, accountId);
        pstmt.setDouble(3, transactionAmount);
        pstmt.setInt(4, transaction_status);
        pstmt.executeUpdate();
        pstmt.close();
    }

    private List<Map<String, Object>> getSettledHistory(PreparedStatement pstmt, int accountId, int historyId) {
        try {
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, historyId);
            return resultSetToList(pstmt);
        } catch (SQLException ex) {
            return new ArrayList<Map<String, Object>>();
        }
    }

    private List<Map<String, Object>> getHistories(PreparedStatement pstmt, int accountId, int balanceHistoryId, int historyId) {
        try {
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, balanceHistoryId);
            pstmt.setInt(3, historyId);
            return resultSetToList(pstmt);
        } catch (SQLException ex) {
            return new ArrayList<Map<String, Object>>();
        }
    }

    private List<Map<String, Object>> resultSetToList(PreparedStatement pstmt) throws SQLException {
        ResultSet rs = pstmt.executeQuery();
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<String, Object>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i).toLowerCase(), rs.getObject(i));
            }
            rows.add(row);
        }
        rs.close();
        return rows;
    }

    private Map<String, Object> resultSetToMap(PreparedStatement pstmt) throws SQLException {
        ResultSet rs = pstmt.executeQuery();
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        Map<String, Object> row = null;
        while (rs.next()) {
            row = new HashMap<String, Object>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i).toLowerCase(), rs.getObject(i));
            }
        }
        rs.close();
        return row;
    }

    private void updateTxnStatus(Connection conn, PreparedStatement pstmt, int accountId, int historyId, int prevHistoryId, int dataListSize, int transactionStatus, Double previousBalanceAmount, Double balanceAmount) throws SQLException {
        pstmt = conn.prepareStatement("blind update " + BWPConstant.TRANSACTION_TABLE + " set prev_history_id=?,data_list_size=?,transaction_status=?,previous_balance_amount=?, balance_amount=?, last_updated_date=CURRENT_TIMESTAMP where account_id=? and history_id=?");
        pstmt.setInt(1, prevHistoryId);
        pstmt.setInt(2, dataListSize);
        pstmt.setInt(3, transactionStatus);
        pstmt.setDouble(4, previousBalanceAmount);
        pstmt.setDouble(5, balanceAmount);
        pstmt.setInt(6, accountId);
        pstmt.setInt(7, historyId);
        pstmt.executeUpdate();
        pstmt.close();
    }

    public void insertHistoryTable(Connection conn, PreparedStatement pstmt, int accountId, int historyId, int prevHistoryId, int dataListSize, int transactionStatus, Double previousBalanceAmount, Double balanceAmount, Double transactionAmount) throws SQLException {
        pstmt = conn.prepareStatement("blind insert into " + BWPConstant.HISTORY_TABLE + "(history_id,prev_history_id,data_list_size,account_id,transaction_amount,previous_balance_amount,balance_amount,transaction_date, last_updated_date, transaction_status) values (?,?,?,?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?)");
        pstmt.setInt(1, historyId);
        pstmt.setInt(2, prevHistoryId);
        pstmt.setInt(3, dataListSize);
        pstmt.setInt(4, accountId);
        pstmt.setDouble(5, transactionAmount);
        pstmt.setDouble(6, previousBalanceAmount);
        pstmt.setDouble(7, balanceAmount);
        pstmt.setInt(8, transactionStatus);
        pstmt.executeUpdate();
        pstmt.close();
    }

    private int getMax(PreparedStatement pstmt, int accountId, int historyId) throws SQLException {
        int maxHistoryId = 0;
        pstmt.setInt(1, accountId);
        pstmt.setInt(2, historyId);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            maxHistoryId = rs.getInt("max_");
        }
        rs.close();
        return maxHistoryId;
    }

    private int getMin(PreparedStatement pstmt, int accountId, int historyId) {
        int minHistoryId = -1;
        try {
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, historyId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                minHistoryId = rs.getInt("min_");
            }
            rs.close();
        } catch (SQLException e) {
        }
        return minHistoryId;
    }

    private void deleteTxnTable(Connection conn, PreparedStatement pstmt, int accountId, int historyId) throws SQLException {
        pstmt = conn.prepareStatement("blind delete from " + BWPConstant.TRANSACTION_TABLE + " a where a.account_id=? and a.history_id<?");
        pstmt.setInt(1, accountId);
        pstmt.setInt(2, historyId);
        pstmt.executeUpdate();
        pstmt.close();
    }

    private int updateBalanceTbl(Connection conn, PreparedStatement pstmt, int accountId, int historyId, Double balanceAmount) throws SQLException {
        //pstmt = conn.prepareStatement("blind update " + BWPConstant.BALANCE_TABLE + " b set b.last_updated_date=CURRENT_TIMESTAMP,b.history_id=?,b.balance_amount=? where b.account_id=? and (select max(h.history_id) from " + BWPConstant.TRANSACTION_TABLE + " h where h.account_id=? and  h.history_id>=? and h.transaction_status!=" + BWPConstant.NOT_APPROVED + ")=?");
        pstmt = conn.prepareStatement("blind update " + BWPConstant.BALANCE_TABLE + " b set b.last_updated_date=CURRENT_TIMESTAMP,b.history_id=?,b.balance_amount=? where b.account_id=? and (select count(h.history_id) from " + BWPConstant.TRANSACTION_TABLE + " h where h.account_id=? and h.history_id>?)=0");
        pstmt.setInt(1, historyId);
        pstmt.setDouble(2, balanceAmount);
        pstmt.setInt(3, accountId);
        pstmt.setInt(4, accountId);
        pstmt.setInt(5, historyId);
        int rowcount = pstmt.executeUpdate();
        pstmt.close();
        return rowcount;
    }
}
