/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blindwriteprotocol.consistency.function;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.derby.jdbc.ClientDriver;
import org.blindwriteprotocol.consistency.init.CreateTableAndSequence;

/**
 *
 * @author khairul.a
 */
public class TestBlindWrite {

    private final String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
    private int numberTransaction = 0;
    private int maxNumberThreadAllowed = 1;
    private int numberActiveThread = 0;
    private Driver d = null;

    public static void main(String[] args) throws SQLException, InterruptedException {
        CreateTableAndSequence.init(Double.parseDouble("1000"));
        TestBlindWrite obj = new TestBlindWrite();
        obj.runTesting(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }

    public void runTesting(int a, int b) throws SQLException, InterruptedException {
        maxNumberThreadAllowed = a;
        numberTransaction = b;
        d = new ClientDriver();
        String accountNumber = BWPConstant.FIRST_ACCOUNT_NUMBER;
        Random rand = new Random();
        String[] operator = {"-", "+"};
        for (int i = 1; i <= numberTransaction; i++) {
            while (numberActiveThread >= maxNumberThreadAllowed) {
                BWPConstant.sleep(10);
            }
            numberActiveThread++;
            Double transactionAmount = Double.parseDouble(getRandom(rand, operator) + "100");
            runInThread(accountNumber, transactionAmount);
        }
    }

    private String getRandom(Random rand, String[] operator) {
        int rdm = rand.nextInt(operator.length);
        return operator[rdm];
    }

    private void runInThread(String accountNumber, Double transactionAmount) {
        Runnable r = new Runnable() {
            public void run() {
                blindWriteOperation(accountNumber, transactionAmount);
            }
        };
        new Thread(r).start();
    }

    private void blindWriteOperation(String accountNumber, Double transactionAmount) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            aggregation(conn, pstmt, rs, accountNumber, transactionAmount);
        } catch (Exception e) {
            System.out.println("blindWriteOperation::e::" + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
            }
            if (numberActiveThread > 0) {
                numberActiveThread--;
            }
        }
    }

    private void aggregation(Connection conn, PreparedStatement pstmt, ResultSet rs, String accountNumber, Double transactionAmount) throws SQLException {
        Map<String, Object> balanceRow = getAccoutId(conn, pstmt, accountNumber);
        int accountId = (int) balanceRow.get("account_id");
        int historyId = getSequence(conn, pstmt, rs, BWPConstant.HISTORY_ID_SEQUENCE + accountId);
        insertTxnTable(conn, pstmt, rs, accountId, historyId, transactionAmount, BWPConstant.NOT_APPROVED);
        Double resultBalanceAmount = transact(conn, pstmt, rs, historyId, accountId, transactionAmount, balanceRow);
        if ((resultBalanceAmount.compareTo(Double.parseDouble("0")) >= 0)) {
            updateBalanceTbl(conn, pstmt, accountId, historyId, resultBalanceAmount);
        }
    }

    private Double transact(Connection conn, PreparedStatement pstmt, ResultSet rs, int historyId, int accountId, Double transactionAmount, Map<String, Object> balanceRow) throws SQLException {
        int count_ = countTxn(conn, pstmt, rs, accountId, historyId);
        if (count_ > 0) {
            while (countHistory(conn, pstmt, rs, accountId, historyId) == 0) {
                BWPConstant.sleep(100);
            }
            return Double.parseDouble("-1");
        }
        Map<String, Object> balanceRow_ = getSettledHistory(conn, pstmt, accountId, historyId);
        if (balanceRow_ != null) {
            balanceRow = balanceRow_;
            balanceRow_ = null;
        }
        Double previousBalanceAmount = Double.parseDouble(balanceRow.get("balance_amount").toString());
        int prevHistoryId = (int) balanceRow.get("history_id");
        List<Map<String, Object>> dataTransaction = getHistories(conn, pstmt, accountId, prevHistoryId, historyId);
        boolean isNotApproved = true;
        while ((historyId - prevHistoryId - dataTransaction.size()) > 1 && isNotApproved) {
            BWPConstant.sleep(100);
            if (conn.isClosed()) {
                conn = getConnection();
            }
            isNotApproved = isNotApproved(conn, pstmt, rs, accountId, historyId);
            if (isNotApproved) {
                balanceRow = getSettledHistory(conn, pstmt, accountId, historyId);
                if (balanceRow != null) {
                    previousBalanceAmount = Double.parseDouble(balanceRow.get("balance_amount").toString());
                    prevHistoryId = (int) balanceRow.get("history_id");
                }
                dataTransaction = getHistories(conn, pstmt, accountId, prevHistoryId, historyId);
            }
        }
        if (!isNotApproved) {
            return Double.parseDouble("-1");
        }
        for (Map<String, Object> row : dataTransaction) {
            int history_id = (int) row.get("history_id");
            Double transaction_amount = Double.parseDouble(row.get("transaction_amount").toString());
            Double temp = Double.sum(previousBalanceAmount, transaction_amount);
            if (temp.compareTo(Double.parseDouble("0")) >= 0) {
                updateTxnStatus(conn, pstmt, accountId, history_id, prevHistoryId, (history_id - prevHistoryId - 1), BWPConstant.APPROVED, transaction_amount, previousBalanceAmount, temp);
                previousBalanceAmount = temp;
            } else {
                updateTxnStatus(conn, pstmt, accountId, history_id, prevHistoryId, (history_id - prevHistoryId - 1), BWPConstant.REJECTED, transaction_amount, previousBalanceAmount, Double.parseDouble("0"));
            }
        }
        Double resultBalanceAmount = Double.sum(previousBalanceAmount, transactionAmount);
        int transactionStatus = BWPConstant.REJECTED;
        Double balanceAmount = Double.parseDouble("0");
        if ((resultBalanceAmount.compareTo(Double.parseDouble("0")) >= 0)) {
            transactionStatus = BWPConstant.APPROVED;
            balanceAmount = resultBalanceAmount;
        }
        updateTxnStatus(conn, pstmt, accountId, historyId, prevHistoryId, dataTransaction.size(), transactionStatus, transactionAmount, previousBalanceAmount, balanceAmount);
        deleteTxnTable(conn, pstmt, rs, accountId, prevHistoryId);
        return balanceAmount;
    }

    private Connection getConnection() {
        try {
            return d.connect(url, null);
        } catch (SQLException ex) {
            BWPConstant.sleep(100);
            return getConnection();
        }
    }

    private String preventSqlInjection(String SequenceName) {
        /*prevent sql injection*/
        SequenceName = SequenceName.replaceAll(" ", "");
        SequenceName = SequenceName.replaceAll(",", "");
        SequenceName = SequenceName.replaceAll(";", "");
        SequenceName = SequenceName.replaceAll("-", "");
        SequenceName = SequenceName.replaceAll("/", "");
        return SequenceName;
    }

    private int getSequence(Connection conn, PreparedStatement pstmt, ResultSet rs, String SequenceName) {
        int seq_id = -1;
        try {
            SequenceName = preventSqlInjection(SequenceName);
            String selectSQL = "select NEXT VALUE FOR " + SequenceName + " as seq_id from SYSIBM.SYSDUMMY1";
            pstmt = conn.prepareStatement(selectSQL);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                seq_id = rs.getInt("seq_id");
            }
        } catch (SQLException ex) {
        }
        return seq_id;
    }

    private Map<String, Object> getAccoutId(Connection conn, PreparedStatement pstmt, String accountNumber) {
        try {
            pstmt = conn.prepareStatement("select account_id,balance_amount,history_id from " + BWPConstant.BALANCE_TABLE + " where account_number=?");
            pstmt.setString(1, accountNumber);
            return resultSetToMap(pstmt.executeQuery());
        } catch (SQLException ex) {
        }
        return null;
    }

    private void insertTxnTable(Connection conn, PreparedStatement pstmt, ResultSet rs, int accountId, int historyId, Double transactionAmount, int transaction_status) {
        try {
            pstmt = conn.prepareStatement("blind insert into " + BWPConstant.TRANSACTION_TABLE + "(history_id, account_id,transaction_amount,transaction_date,last_updated_date,transaction_status) values (?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?)");
            pstmt.setInt(1, historyId);
            pstmt.setInt(2, accountId);
            pstmt.setDouble(3, transactionAmount);
            pstmt.setInt(4, transaction_status);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
        }
    }

    private boolean isNotApproved(Connection conn, PreparedStatement pstmt, ResultSet rs, int accountId, int historyId) {
        int transactionStatus = -1;
        try {
            pstmt = conn.prepareStatement("select transaction_status from " + BWPConstant.TRANSACTION_TABLE + " b where b.account_id=? and b.history_id=?");
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, historyId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                transactionStatus = rs.getInt("transaction_status");
            }
        } catch (SQLException ex) {
            try {
                if (conn.isClosed()) {
                    return true;
                }
            } catch (SQLException ex1) {
            }
        }
        return (transactionStatus == BWPConstant.NOT_APPROVED);
    }

    private Map<String, Object> getSettledHistory(Connection conn, PreparedStatement pstmt, int accountId, int historyId) {
        try {
            pstmt = conn.prepareStatement("select history_id,balance_amount from " + BWPConstant.TRANSACTION_TABLE + " h where h.account_id=? and h.history_id=(select max(hh.history_id) from " + BWPConstant.TRANSACTION_TABLE + " hh where hh.account_id=? and hh.transaction_status!=" + BWPConstant.NOT_APPROVED + " and hh.history_id<? )");
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, historyId);
            return resultSetToMap(pstmt.executeQuery());
        } catch (SQLException ex) {
        }
        return null;
    }

    private List<Map<String, Object>> getHistories(Connection conn, PreparedStatement pstmt, int accountId, int balanceHistoryId, int historyId) {
        try {
            pstmt = conn.prepareStatement("select history_id,transaction_amount from " + BWPConstant.TRANSACTION_TABLE + " h where h.account_id=? and h.history_id>? and h.history_id<? order by history_id asc");
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, balanceHistoryId);
            pstmt.setInt(3, historyId);
            return resultSetToList(pstmt.executeQuery());
        } catch (SQLException ex) {
            return new ArrayList<Map<String, Object>>();
        }
    }

    private List<Map<String, Object>> resultSetToList(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<String, Object>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i).toLowerCase(), rs.getObject(i));
            }
            rows.add(row);
        }
        return rows;
    }

    private Map<String, Object> resultSetToMap(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        Map<String, Object> row = null;
        while (rs.next()) {
            row = new HashMap<String, Object>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i).toLowerCase(), rs.getObject(i));
            }
        }
        return row;
    }

    private void updateTxnStatus(Connection conn, PreparedStatement pstmt, int accountId, int historyId, int prevHistoryId, int dataListSize, int transactionStatus, Double transactionAmount, Double previousBalanceAmount, Double balanceAmount) {
        try {
            pstmt = conn.prepareStatement("blind update " + BWPConstant.TRANSACTION_TABLE + " set prev_history_id=?,data_list_size=?,transaction_status=?,previous_balance_amount=?, balance_amount=?, last_updated_date=CURRENT_TIMESTAMP where account_id=? and transaction_status=" + BWPConstant.NOT_APPROVED + " and history_id=?");
            pstmt.setInt(1, prevHistoryId);
            pstmt.setInt(2, dataListSize);
            pstmt.setInt(3, transactionStatus);
            pstmt.setDouble(4, previousBalanceAmount);
            pstmt.setDouble(5, balanceAmount);
            pstmt.setInt(6, accountId);
            pstmt.setInt(7, historyId);
            pstmt.executeUpdate();
            insertHistoryTable(conn, pstmt, accountId, historyId, prevHistoryId, dataListSize, transactionAmount, previousBalanceAmount, balanceAmount, transactionStatus);
        } catch (SQLException ex) {
        }
    }

    public void insertHistoryTable(Connection conn, PreparedStatement pstmt, int accountId, int historyId, int prevHistoryId, int dataListSize, Double transactionAmount, Double previousBalanceAmount, Double balanceAmount, int transactionStatus) {
        try {
            pstmt = conn.prepareStatement("blind insert into " + BWPConstant.HISTORY_TABLE + "(history_id,prev_history_id,data_list_size,account_id,transaction_amount,previous_balance_amount,balance_amount,transaction_date, last_updated_date, transaction_status) values (?,?,?,?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?)");
            pstmt.setInt(1, historyId);
            pstmt.setInt(2, prevHistoryId);
            pstmt.setInt(3, dataListSize);
            pstmt.setInt(4, accountId);
            pstmt.setDouble(5, transactionAmount);
            pstmt.setDouble(6, previousBalanceAmount);
            pstmt.setDouble(7, balanceAmount);
            pstmt.setInt(8, transactionStatus);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
        }
    }

    private int countTxn(Connection conn, PreparedStatement pstmt, ResultSet rs, int accountId, int historyId) {
        int count_id = -1;
        try {
            pstmt = conn.prepareStatement("select count(b.history_id) count_ from " + BWPConstant.TRANSACTION_TABLE + " b where b.account_id=? and b.history_id>?");
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, historyId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                count_id = rs.getInt("count_");
            }
        } catch (SQLException ex) {
        }
        return count_id;
    }

    private int countHistory(Connection conn, PreparedStatement pstmt, ResultSet rs, int accountId, int historyId) {
        int count_id = -1;
        try {
            pstmt = conn.prepareStatement("select count(b.history_id) count_ from " + BWPConstant.HISTORY_TABLE + " b where b.account_id=? and b.history_id=?");
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, historyId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                count_id = rs.getInt("count_");
            }
        } catch (SQLException ex) {
        }
        return count_id;
    }

    private void deleteTxnTable(Connection conn, PreparedStatement pstmt, ResultSet rs, int accountId, int deletedHistoryId) {
        try {
            pstmt = conn.prepareStatement("blind delete from " + BWPConstant.TRANSACTION_TABLE + " where account_id=? and history_id<?");
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, deletedHistoryId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
        }
    }

    private void updateBalanceTbl(Connection conn, PreparedStatement pstmt, int accountId, int historyId, Double balanceAmount) {
        try {
            pstmt = conn.prepareStatement("blind update " + BWPConstant.BALANCE_TABLE + " b set b.last_updated_date=CURRENT_TIMESTAMP,b.history_id=?,b.balance_amount=? where b.account_id=? and (select max(h.history_id) from " + BWPConstant.TRANSACTION_TABLE + " h where h.account_id=? and h.transaction_status!=" + BWPConstant.NOT_APPROVED + " and  h.history_id>=?)=?");
            pstmt.setInt(1, historyId);
            pstmt.setDouble(2, balanceAmount);
            pstmt.setInt(3, accountId);
            pstmt.setInt(4, accountId);
            pstmt.setInt(5, historyId);
            pstmt.setInt(6, historyId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
        }
    }
}
