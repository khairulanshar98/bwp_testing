/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blindwriteprotocol.consistency.function;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import org.apache.derby.jdbc.ClientDriver;

/**
 *
 * @author khairul.a
 */
public class GetConnectionOnly {

    private final String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
    private int numberTransaction = 0;
    private int maxNumberThreadAllowed = 1;
    private ArrayList numberActiveThread = new ArrayList();
    private Driver d = null;
    private static int totalTime = -2;
    private static final ArrayList responseTime = new ArrayList();
    private static ArrayList HistoryIdList = new ArrayList();

    public static void main(String[] args) throws SQLException, InterruptedException {
        GetConnectionOnly obj = new GetConnectionOnly();
        obj.maxNumberThreadAllowed = Integer.parseInt(args[0]);
        obj.numberTransaction = Integer.parseInt(args[1]);
        obj.d = new ClientDriver();
        for (int i = 1; i <= 24; i++) {
            obj.runTesting(i);
        }
        double avg = totalTime / responseTime.size();
        double throughput = 1000 * obj.numberTransaction / avg;
        double sd = 0;
        long minTime = 10000000;
        long maxTime = 0;
        for (int i = 0; i < responseTime.size(); i++) {
            long diff = (long) responseTime.get(i);
            if (diff < minTime) {
                minTime = diff;
            }
            if (diff > maxTime) {
                maxTime = diff;
            }
            sd += Math.pow(diff - avg, 2);
        }
        sd = Math.sqrt(sd / 21);
        System.out.println("totalTime::" + totalTime + "::avg::" + avg + "::minTime::" + minTime + "::maxTime::" + maxTime + "::sd::" + sd + "::throughput::" + throughput);
    }

    public void runTesting(int testNo) throws SQLException, InterruptedException {
        HistoryIdList = new ArrayList();
        numberActiveThread = new ArrayList();
        BWPConstant.sleep(15000);
        Date start = new Date();
        for (int i = 1; i <= numberTransaction; i++) {
            while (numberActiveThread.size() >= maxNumberThreadAllowed) {
                BWPConstant.sleep(10);
            }
            numberActiveThread.add(1);
            runInThread();
        }
        while (HistoryIdList.size() < numberTransaction) {
            BWPConstant.sleep(100);
        }
        Date end = new Date();
        long diff = end.getTime() - start.getTime();
        System.out.println("testNo::" + testNo + "::response time::" + diff);
        switch (totalTime) {
            case -3:
                totalTime = -2;
                break;
            case -2:
                totalTime = -1;
                break;
            case -1:
                totalTime = 0;
                break;
            default:
                totalTime += diff;
                responseTime.add(diff);
                break;
        }
    }

    private void runInThread() {
        Runnable r = new Runnable() {
            public void run() {
                blindWriteOperation();
            }
        };
        new Thread(r).start();
    }

    private void blindWriteOperation() {
        try {
            Connection conn = getConnection();
            conn.close();
        } catch (SQLException ex) {
        }
        synchronized (this) {
            if (numberActiveThread.size() > 0) {
                numberActiveThread.remove(numberActiveThread.size() - 1);
            }
            HistoryIdList.add(1);
        }
    }

    private Connection getConnection() {
        try {
            return d.connect(url, null);
        } catch (SQLException ex) {
            BWPConstant.sleep(10);
            return getConnection();
        }
    }
}
