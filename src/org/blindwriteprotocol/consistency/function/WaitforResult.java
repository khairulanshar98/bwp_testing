/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blindwriteprotocol.consistency.function;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.derby.jdbc.ClientDriver;

/**
 *
 * @author khairul.a
 */
public class WaitforResult {

    private final String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
    private int numberTransaction = 0;
    private int maxNumberThreadAllowed = 1;
    private ArrayList numberActiveThread = new ArrayList();
    private Driver d = null;
    private static int totalTime = -2;
    private static final ArrayList responseTime = new ArrayList();
    private static ArrayList HistoryIdList = new ArrayList();
    private static final Random rand = new Random();
    private static final String[] operator = {"-", "+"};

    public static void main(String[] args) throws SQLException, InterruptedException {
        WaitforResult obj = new WaitforResult();
        obj.maxNumberThreadAllowed = Integer.parseInt(args[0]);
        obj.numberTransaction = Integer.parseInt(args[1]);
        obj.d = new ClientDriver();
        for (int i = 1; i <= 24; i++) {
            obj.runTesting(i);
        }
        double avg = totalTime / responseTime.size();
        double throughput = 1000 * obj.numberTransaction / avg;
        double sd = 0;
        long minTime = 10000000;
        long maxTime = 0;
        for (int i = 0; i < responseTime.size(); i++) {
            long diff = (long) responseTime.get(i);
            if (diff < minTime) {
                minTime = diff;
            }
            if (diff > maxTime) {
                maxTime = diff;
            }
            sd += Math.pow(diff - avg, 2);
        }
        sd = Math.sqrt(sd / 21);
        System.out.println("totalTime::" + totalTime + "::avg::" + avg + "::minTime::" + minTime + "::maxTime::" + maxTime + "::sd::" + sd + "::throughput::" + throughput);
    }

    public void runTesting(int testNo) throws SQLException, InterruptedException {
        String accountNumber = BWPConstant.FIRST_ACCOUNT_NUMBER;
        HistoryIdList = new ArrayList();
        numberActiveThread = new ArrayList();
        BWPConstant.sleep(60000);
        Date start = new Date();
        for (int i = 1; i <= numberTransaction; i++) {
            while (numberActiveThread.size() >= maxNumberThreadAllowed) {
                BWPConstant.sleep(10);
            }
            numberActiveThread.add(1);
            Double transactionAmount = Double.parseDouble(getRandom() + "100");
            runInThread(accountNumber, transactionAmount);
        }
        while (HistoryIdList.size() < numberTransaction) {
            BWPConstant.sleep(100);
        }
        Date end = new Date();
        long diff = end.getTime() - start.getTime();
        System.out.println("testNo::" + testNo + "::response time::" + diff);
        switch (totalTime) {
            case -3:
                totalTime = -2;
                break;
            case -2:
                totalTime = -1;
                break;
            case -1:
                totalTime = 0;
                break;
            default:
                totalTime += diff;
                responseTime.add(diff);
                break;
        }
    }

    private String getRandom() {
        int rdm = rand.nextInt(operator.length);
        return operator[rdm];
    }

    private void runInThread(String accountNumber, Double transactionAmount) {
        Runnable r = new Runnable() {
            public void run() {
                blindWriteOperation(accountNumber, transactionAmount);
            }
        };
        new Thread(r).start();
    }

    private void blindWriteOperation(String accountNumber, Double transactionAmount) {
        int historyId = aggregation(accountNumber, transactionAmount);
        synchronized (this) {
            if (numberActiveThread.size() > 0) {
                numberActiveThread.remove(numberActiveThread.size() - 1);
            }
            HistoryIdList.add(historyId);
        }
    }

    private int aggregation(String accountNumber, Double transactionAmount) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        int historyId = 0;
        try {
            conn = getConnection();
            historyId = getSequence(conn, BWPConstant.HISTORY_ID_SEQUENCE + accountNumber);
            Map<String, Object> balanceRow = getAccoutId(conn, pstmt, accountNumber);
            int accountId = (int) balanceRow.get("account_id");
            insertHistoryTable(conn, pstmt, accountId, historyId, transactionAmount, BWPConstant.NOT_APPROVED);
            PreparedStatement pstmtMaxTxn = conn.prepareStatement("select b.history_id from " + BWPConstant.HISTORY_TABLE + " b where b.account_id=? and b.history_id>? FETCH FIRST 1 ROWS ONLY");
            //PreparedStatement pstmtMaxTxn = conn.prepareStatement("VALUES SYSCS_UTIL.SYSCS_PEEK_AT_SEQUENCE('APP', ?)-1");
            boolean waitForResult = true;
            if (getMax(pstmtMaxTxn, accountId, historyId) == historyId) {
                //if (getMax(pstmtMaxTxn, BWPConstant.HISTORY_ID_SEQUENCE + accountNumber) == historyId) {
                PreparedStatement pstmtupdateHistory = conn.prepareStatement("blind update " + BWPConstant.HISTORY_TABLE + " set transaction_status=?,previous_balance_amount=?, balance_amount=?, last_updated_date=CURRENT_TIMESTAMP where account_id=? and history_id=?");
                Double previousBalanceAmount = get_previous_balance_amount(conn, pstmtMaxTxn, pstmtupdateHistory, accountNumber, historyId, accountId, balanceRow);
                if (previousBalanceAmount != null) {
                    Double newBalanceAmount = Double.sum(previousBalanceAmount, transactionAmount);
                    int transactionStatus = BWPConstant.APPROVED;
                    if ((newBalanceAmount.compareTo(Double.parseDouble("0")) < 0)) {
                        newBalanceAmount = previousBalanceAmount;
                        transactionStatus = BWPConstant.REJECTED;
                    }
                    updateHistoryStatus(pstmtupdateHistory, accountId, historyId, transactionStatus, previousBalanceAmount, newBalanceAmount);
                    if (getMax(pstmtMaxTxn, accountId, historyId) == historyId) {
                        //if (getMax(pstmtMaxTxn, BWPConstant.HISTORY_ID_SEQUENCE + accountNumber) == historyId) {

                        updateBalanceTbl(conn, pstmt, accountId, historyId, newBalanceAmount);
                        waitForResult = false;
                    }
                }
                pstmtupdateHistory.close();
            }
            if (waitForResult) {
                PreparedStatement pstmtCekTransactionStatus = conn.prepareStatement("select transaction_status from " + BWPConstant.HISTORY_TABLE + " b where b.account_id=? and b.history_id=?");
                while (cekTransactionStatus(pstmtCekTransactionStatus, accountId, historyId) == BWPConstant.NOT_APPROVED) {
                    BWPConstant.sleep(400);
                }
                pstmtCekTransactionStatus.close();
            }
            pstmtMaxTxn.close();
        } catch (SQLException ex) {
            System.out.println("aggregation::historyId::" + historyId + "::ex::" + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
            }
        }
        return historyId;
    }

    private Double get_previous_balance_amount(Connection conn, PreparedStatement pstmtMaxTxn, PreparedStatement pstmtupdateHistory, String accountNumber, int historyId, int accountId, Map<String, Object> balanceRow) throws SQLException {
        int prevHistoryId = (int) balanceRow.get("history_id");
        List<Map<String, Object>> dataTransaction = new ArrayList<Map<String, Object>>();
        if (historyId - prevHistoryId > 1) {
            PreparedStatement pstmtGetHistories = conn.prepareStatement("select history_id,transaction_amount,transaction_status from " + BWPConstant.HISTORY_TABLE + " h where h.account_id=? and h.history_id>? and h.history_id<? order by history_id asc");
            boolean keep_loop = true;
            int maxHistoryId = historyId;
            while (keep_loop) {
                BWPConstant.sleep(300);
                maxHistoryId = getMax(pstmtMaxTxn, accountId, historyId);
                //maxHistoryId = getMax(pstmtMaxTxn, BWPConstant.HISTORY_ID_SEQUENCE + accountNumber);
                if (maxHistoryId == historyId) {
                    dataTransaction = getHistories(pstmtGetHistories, accountId, prevHistoryId, historyId);
                    if (dataTransaction.size() == historyId - prevHistoryId - 1) {
                        keep_loop = false;
                    }
                } else {
                    keep_loop = false;
                }
            }
            pstmtGetHistories.close();
            if (maxHistoryId != historyId) {
                return null;
            }
        }
        //if (getMax(pstmtMaxTxn, BWPConstant.HISTORY_ID_SEQUENCE + accountNumber) != historyId) {
        if (getMax(pstmtMaxTxn, accountId, historyId) != historyId) {
            return null;
        }
        Double previousBalanceAmount = Double.parseDouble(balanceRow.get("balance_amount").toString());
        for (Map<String, Object> row : dataTransaction) {
            Double transaction_amount = Double.parseDouble(row.get("transaction_amount").toString());
            int history_id = (int) row.get("history_id");
            int transactionStatus = (int) row.get("transaction_status");
            Double temp = Double.sum(previousBalanceAmount, transaction_amount);
            if (temp.compareTo(Double.parseDouble("0")) >= 0) {
                if (transactionStatus == BWPConstant.NOT_APPROVED) {
                    updateHistoryStatus(pstmtupdateHistory, accountId, history_id, BWPConstant.APPROVED, previousBalanceAmount, temp);
                }
                previousBalanceAmount = temp;
            } else {
                if (transactionStatus == BWPConstant.NOT_APPROVED) {
                    updateHistoryStatus(pstmtupdateHistory, accountId, history_id, BWPConstant.REJECTED, previousBalanceAmount, previousBalanceAmount);
                }
            }
        }
        return previousBalanceAmount;
    }

    private Connection getConnection() {
        try {
            return d.connect(url, null);
        } catch (SQLException ex) {
            BWPConstant.sleep(100);
            return getConnection();
        }
    }

    private String preventSqlInjection(String SequenceName) {
        /*prevent sql injection*/
        SequenceName = SequenceName.replaceAll(" ", "");
        SequenceName = SequenceName.replaceAll(",", "");
        SequenceName = SequenceName.replaceAll(";", "");
        SequenceName = SequenceName.replaceAll("-", "");
        SequenceName = SequenceName.replaceAll("/", "");
        return SequenceName;
    }

    private int getSequence(Connection conn, String SequenceName) throws SQLException {
        int seq_id = -1;
        SequenceName = preventSqlInjection(SequenceName);
        PreparedStatement pstmt = conn.prepareStatement("select NEXT VALUE FOR " + SequenceName + " as seq_id from SYSIBM.SYSDUMMY1");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            seq_id = rs.getInt("seq_id");
        }
        pstmt.close();
        return seq_id;
    }

    private Map<String, Object> getAccoutId(Connection conn, PreparedStatement pstmt, String accountNumber) throws SQLException {
        pstmt = conn.prepareStatement("select account_id,balance_amount,history_id from " + BWPConstant.BALANCE_TABLE + " where account_number=?");
        pstmt.setString(1, accountNumber);
        return resultSetToMap(pstmt);
    }

    private void insertHistoryTable(Connection conn, PreparedStatement pstmt, int accountId, int historyId, Double transactionAmount, int transaction_status) throws SQLException {
        pstmt = conn.prepareStatement("blind insert into " + BWPConstant.HISTORY_TABLE + "(history_id, account_id,transaction_amount,transaction_date,last_updated_date,transaction_status) values (?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?)");
        pstmt.setInt(1, historyId);
        pstmt.setInt(2, accountId);
        pstmt.setDouble(3, transactionAmount);
        pstmt.setInt(4, transaction_status);
        pstmt.executeUpdate();
        pstmt.close();
    }

    private List<Map<String, Object>> getHistories(PreparedStatement pstmt, int accountId, int balanceHistoryId, int historyId) {
        try {
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, balanceHistoryId);
            pstmt.setInt(3, historyId);
            return resultSetToList(pstmt);
        } catch (SQLException ex) {
            return new ArrayList<Map<String, Object>>();
        }
    }

    private List<Map<String, Object>> resultSetToList(PreparedStatement pstmt) throws SQLException {
        ResultSet rs = pstmt.executeQuery();
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<String, Object>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i).toLowerCase(), rs.getObject(i));
            }
            rows.add(row);
        }
        rs.close();
        return rows;
    }

    private Map<String, Object> resultSetToMap(PreparedStatement pstmt) throws SQLException {
        ResultSet rs = pstmt.executeQuery();
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        Map<String, Object> row = null;
        while (rs.next()) {
            row = new HashMap<String, Object>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i).toLowerCase(), rs.getObject(i));
            }
        }
        rs.close();
        return row;
    }

    private void updateHistoryStatus(PreparedStatement pstmt, int accountId, int historyId, int transactionStatus, Double previousBalanceAmount, Double balanceAmount) throws SQLException {
        pstmt.setInt(1, transactionStatus);
        pstmt.setDouble(2, previousBalanceAmount);
        pstmt.setDouble(3, balanceAmount);
        pstmt.setInt(4, accountId);
        pstmt.setInt(5, historyId);
        pstmt.executeUpdate();
    }

    public void insertHistoryTable(Connection conn, PreparedStatement pstmt, int accountId, int historyId, int prevHistoryId, int dataListSize, int transactionStatus, Double previousBalanceAmount, Double balanceAmount, Double transactionAmount) throws SQLException {
        pstmt = conn.prepareStatement("blind insert into " + BWPConstant.HISTORY_TABLE + "(history_id,prev_history_id,data_list_size,account_id,transaction_amount,previous_balance_amount,balance_amount,transaction_date, last_updated_date, transaction_status) values (?,?,?,?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?)");
        pstmt.setInt(1, historyId);
        pstmt.setInt(2, prevHistoryId);
        pstmt.setInt(3, dataListSize);
        pstmt.setInt(4, accountId);
        pstmt.setDouble(5, transactionAmount);
        pstmt.setDouble(6, previousBalanceAmount);
        pstmt.setDouble(7, balanceAmount);
        pstmt.setInt(8, transactionStatus);
        pstmt.executeUpdate();
        pstmt.close();
    }

    private int updateBalanceTbl(Connection conn, PreparedStatement pstmt, int accountId, int historyId, Double balanceAmount) throws SQLException {
        pstmt = conn.prepareStatement("blind update " + BWPConstant.BALANCE_TABLE + " b set b.last_updated_date=CURRENT_TIMESTAMP,b.history_id=?,b.balance_amount=? where b.account_id=?");
        pstmt.setInt(1, historyId);
        pstmt.setDouble(2, balanceAmount);
        pstmt.setInt(3, accountId);
        int rowcount = pstmt.executeUpdate();
        pstmt.close();
        return rowcount;
    }

    private int getMax(PreparedStatement pstmt, int accountId, int historyId) throws SQLException {
        int maxHistoryId = historyId;
        pstmt.setInt(1, accountId);
        pstmt.setInt(2, historyId);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            maxHistoryId = rs.getInt("history_id");
        }
        rs.close();
        return maxHistoryId;
    }

    /*private int getMax(PreparedStatement pstmt, String seqName) throws SQLException {
        int maxHistoryId = 0;
        pstmt.setString(1, preventSqlInjection(seqName.toUpperCase()));
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            maxHistoryId = rs.getInt(1);
        }
        rs.close();
        return maxHistoryId;
    }*/
    private int cekTransactionStatus(PreparedStatement pstmt, int accountId, int historyId) throws SQLException {
        int status = BWPConstant.NOT_APPROVED;
        pstmt.setInt(1, accountId);
        pstmt.setInt(2, historyId);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            status = rs.getInt("transaction_status");
        }
        rs.close();
        return status;
    }
}
