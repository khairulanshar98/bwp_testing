/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blindwriteprotocol.consistency.function;

import java.util.UUID;
import org.apache.derby.iapi.util.InterruptStatus;

/**
 *
 * @author khairul.a
 */
public class BWPConstant {

    public static int APPROVED = 1;
    public static int NOT_APPROVED = 0;
    public static int REJECTED = 2;

    public static String ACCOUNT_ID_SEQUENCE = "account_id_seq";
    public static String HISTORY_ID_SEQUENCE = "txn_seq_";

    public static String FIRST_ACCOUNT_NUMBER = "1234-567-890";
    public static String SECOND_ACCOUNT_NUMBER = "0987-654-321";

    public static String BALANCE_TABLE = "balance_tbl";
    public static String HISTORY_TABLE = "history_tbl";
    public static String TRANSACTION_TABLE = "transaction_tbl";

    public static String transactionStatus(int val) {
        if (val == APPROVED) {
            return "Approved";
        }
        if (val == NOT_APPROVED) {
            return "Not Approved";
        }
        if (val == REJECTED) {
            return "Rejected";
        }
        return "";
    }
    
    public static String getUUID(){
        return UUID.randomUUID().toString();
    }
    
    public static void sleep(long millis) {
        long startMillis = System.currentTimeMillis();
        long waited = 0L;
        while (waited < millis) {
            try {
                Thread.sleep(millis - waited);
            } catch (InterruptedException ie) {
                InterruptStatus.setInterrupted();
                waited = System.currentTimeMillis() - startMillis;
                continue;
            }
            break;
        }
    }

}
