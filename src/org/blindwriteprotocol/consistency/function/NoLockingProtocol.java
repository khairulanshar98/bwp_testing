/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blindwriteprotocol.consistency.function;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.apache.derby.jdbc.ClientDriver;

/**
 *
 * @author khairul.a
 */
public class NoLockingProtocol {

    private final String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
    private int numberTransaction = 0;
    private int maxNumberThreadAllowed = 1;
    private ArrayList numberActiveThread = new ArrayList();
    private Driver d = null;
    private static int totalTime = -2;
    private static final List responseTime = new ArrayList();
    private static List HistoryIdList = new ArrayList();
    private static final Random rand = new Random();
    private static final String[] operator = {"-", "+"};

    public static void main(String[] args) throws SQLException, InterruptedException {
        NoLockingProtocol obj = new NoLockingProtocol();
        obj.maxNumberThreadAllowed = Integer.parseInt(args[0]);
        obj.numberTransaction = Integer.parseInt(args[1]);
        obj.d = new ClientDriver();
        for (int i = 1; i <= 24; i++) {
            obj.runTesting(i);
        }
        double avg = totalTime / responseTime.size();
        double throughput = 1000 * obj.numberTransaction / avg;
        double sd = 0;
        long minTime = 10000000;
        long maxTime = 0;
        for (int i = 0; i < responseTime.size(); i++) {
            long diff = (long) responseTime.get(i);
            if (diff < minTime) {
                minTime = diff;
            }
            if (diff > maxTime) {
                maxTime = diff;
            }
            sd += Math.pow(diff - avg, 2);
        }
        sd = Math.sqrt(sd / 22);
        System.out.println("totalTime::" + totalTime + "::avg::" + avg + "::minTime::" + minTime + "::maxTime::" + maxTime + "::sd::" + sd + "::throughput::" + throughput);
    }

    public void runTesting(int testNo) throws SQLException, InterruptedException {
        String accountNumber = BWPConstant.FIRST_ACCOUNT_NUMBER;
        HistoryIdList = new ArrayList();
        numberActiveThread = new ArrayList();
        BWPConstant.sleep(30000);
        Date start = new Date();
        for (int i = 1; i <= numberTransaction; i++) {
            while (numberActiveThread.size() >= maxNumberThreadAllowed) {
                BWPConstant.sleep(10);
            }
            numberActiveThread.add(1);
            Double transactionAmount = Double.parseDouble(getRandom() + "100");
            runInThread(accountNumber, transactionAmount);
        }
        while (HistoryIdList.size() < numberTransaction) {
            BWPConstant.sleep(100);
        }
        Date end = new Date();
        long diff = end.getTime() - start.getTime();
        System.out.println("testNo::" + testNo + "::response time::" + diff);
        switch (totalTime) {
            case -2:
                totalTime = -1;
                break;
            case -1:
                totalTime = 0;
                break;
            default:
                totalTime += diff;
                responseTime.add(diff);
                break;
        }
    }

    private String getRandom() {
        int rdm = rand.nextInt(operator.length);
        return operator[rdm];
    }

    private void runInThread(String accountNumber, Double transactionAmount) {
        Runnable r = new Runnable() {
            public void run() {
                normalWriteOperation(accountNumber, transactionAmount);
            }
        };
        new Thread(r).start();
    }

    private void normalWriteOperation(String accountNumber, Double transactionAmount) {
        int historyId = aggregation(accountNumber, transactionAmount);
        synchronized (this) {
            if (numberActiveThread.size() > 0) {
                numberActiveThread.remove(numberActiveThread.size() - 1);
            }
            HistoryIdList.add(historyId);
        }
    }

    private Connection getConnection() {
        try {
            return d.connect(url, null);
        } catch (SQLException ex) {
            BWPConstant.sleep(100);
            return getConnection();
        }
    }

    private int aggregation(String accountNumber, Double transactionAmount) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        int historyId = 0;
        try {
            conn = getConnection();
            historyId = getSequence(conn, BWPConstant.HISTORY_ID_SEQUENCE + accountNumber);
            ResultSet balanceRs = getBalanceTbl(conn, pstmt, accountNumber);
            while (balanceRs.next()) {
                int accountId = balanceRs.getInt("account_id");
                Double previousBalanceAmount = balanceRs.getDouble("balance_amount");
                Double resultBalanceAmount = Double.sum(previousBalanceAmount, transactionAmount);
                int transactionStatus = BWPConstant.APPROVED;
                if ((resultBalanceAmount.compareTo(Double.parseDouble("0")) < 0)) {
                    transactionStatus = BWPConstant.REJECTED;
                    resultBalanceAmount = previousBalanceAmount;
                }
                updateBalanceTbl(conn, pstmt, historyId, resultBalanceAmount, accountId);
                insertHistoryTable(conn, accountId, historyId, transactionAmount, transactionStatus, previousBalanceAmount, resultBalanceAmount);
            }
        } catch (Exception e) {
            System.out.println("NoLockingProtocol::e::" + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
            }
        }
        return historyId;
    }

    private String preventSqlInjection(String SequenceName) {
        /*prevent sql injection*/
        SequenceName = SequenceName.replaceAll(" ", "");
        SequenceName = SequenceName.replaceAll(",", "");
        SequenceName = SequenceName.replaceAll(";", "");
        SequenceName = SequenceName.replaceAll("-", "");
        SequenceName = SequenceName.replaceAll("/", "");
        return SequenceName;
    }

    private int getSequence(Connection conn, String SequenceName) throws SQLException {
        int seq_id = -1;
        SequenceName = preventSqlInjection(SequenceName);
        PreparedStatement pstmt = conn.prepareStatement("select NEXT VALUE FOR " + SequenceName + " as seq_id from SYSIBM.SYSDUMMY1");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            seq_id = rs.getInt("seq_id");
        }
        return seq_id;
    }

    private ResultSet getBalanceTbl(Connection conn, PreparedStatement pstmt, String accountNumber) throws SQLException {
        pstmt = conn.prepareStatement("select * from " + BWPConstant.BALANCE_TABLE + " where account_number=?");
        pstmt.setString(1, accountNumber);
        return pstmt.executeQuery();
    }

    private void insertHistoryTable(Connection conn, int accountId, int historyId,
            Double transactionAmount, int transaction_status, Double previousBalanceAmount, Double balanceAmount)
            throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement("blind insert into " + BWPConstant.HISTORY_TABLE
                + "(history_id,account_id,transaction_amount,transaction_status,transaction_date,"
                + "last_updated_date,previous_balance_amount,balance_amount) values (?,?,?,?,CURRENT_TIMESTAMP,"
                + "CURRENT_TIMESTAMP,?,?)");
        pstmt.setInt(1, historyId);
        pstmt.setInt(2, accountId);
        pstmt.setDouble(3, transactionAmount);
        pstmt.setInt(4, transaction_status);
        pstmt.setDouble(5, previousBalanceAmount);
        pstmt.setDouble(6, balanceAmount);
        pstmt.executeUpdate();
    }

    public int updateBalanceTbl(Connection conn, PreparedStatement pstmt, int historyId, Double balanceAmount,
            int accountId) throws SQLException {
        pstmt = conn.prepareStatement("blind update " + BWPConstant.BALANCE_TABLE
                + " set last_updated_date=CURRENT_TIMESTAMP, history_id=?, balance_amount=? where account_id=?");
        pstmt.setInt(1, historyId);
        pstmt.setDouble(2, balanceAmount);
        pstmt.setInt(3, accountId);
        return pstmt.executeUpdate();
    }
}
