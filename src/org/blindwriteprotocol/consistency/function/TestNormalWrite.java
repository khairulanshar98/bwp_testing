/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blindwriteprotocol.consistency.function;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import org.apache.derby.jdbc.ClientDriver;
import org.blindwriteprotocol.consistency.init.CreateTableAndSequence;

/**
 *
 * @author khairul.a
 */
public class TestNormalWrite {

    /*
    To test the result please execute this query
    select account_number,balance_amount from balance_tbl;
    select history_id,prev_history_id,data_list_size,previous_balance_amount,transaction_amount,balance_amount,transaction_status status from history_tbl where account_id=(select b.account_id from balance_tbl b where b.account_number='1234-567-890') order by history_id;
     */
    private static TestNormalWrite consistencyTest = null;
    private static final String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
    private static int numberTransaction = 20000;
    private static int maxNumberThreadAllowed = 200;
    private static int numberActiveThread = 0;
    private Driver d = null;

    public static void main(String[] args) throws SQLException, InterruptedException {
        CreateTableAndSequence.init(Double.parseDouble("1000"));
        TestNormalWrite obj = new TestNormalWrite();
        obj.runTesting(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }

    public void runTesting(int a, int b) throws SQLException, InterruptedException {
        maxNumberThreadAllowed = a;
        numberTransaction = b;
        d = new ClientDriver();
        String accountNumber = BWPConstant.FIRST_ACCOUNT_NUMBER;
        Random rand = new Random();
        String[] operator = {"-", "+"};
        for (int i = 1; i <= numberTransaction; i++) {
            while (numberActiveThread >= maxNumberThreadAllowed) {
                BWPConstant.sleep(10);
            }
            numberActiveThread++;
            Double transactionAmount = Double.parseDouble(getRandom(rand, operator) + "100");
            runInThread(i, accountNumber, transactionAmount);
        }
    }

    private String getRandom(Random rand, String[] operator) {
        int rdm = rand.nextInt(operator.length);
        return operator[rdm];
    }

    private void runInThread(int i, String accountNumber, Double transactionAmount) {
        Runnable r = new Runnable() {
            public void run() {
                normalWriteOperation(i, accountNumber, transactionAmount);
            }
        };
        new Thread(r).start();
    }

    private void normalWriteOperation(int i, String accountNumber, Double transactionAmount) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            int accountId = getAccoutId(conn, pstmt, rs, accountNumber);
            aggregation(conn, pstmt, rs, accountId, transactionAmount);
        } catch (Exception e) {
            System.out.println("normalWriteOperation::e::" + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
            }
            if (numberActiveThread > 0) {
                numberActiveThread--;
            }
        }
    }

    private Connection getConnection() {
        try {
            return d.connect(url, null);
        } catch (SQLException ex) {
            BWPConstant.sleep(100);
            return getConnection();
        }
    }

    private void aggregation(Connection conn, PreparedStatement pstmt, ResultSet rs, int accountId,
            Double transactionAmount) throws SQLException {
        int historyId = getSequence(conn, pstmt, rs, BWPConstant.HISTORY_ID_SEQUENCE);
        int noOfRowUpdated = updateBalanceTbl(conn, pstmt, accountId, transactionAmount);
        if (noOfRowUpdated > 0) {
            conn.commit();
            insertHistoryTable(conn, pstmt, rs, accountId, historyId, transactionAmount, BWPConstant.APPROVED);
        } else {
            conn.rollback();
            insertHistoryTable(conn, pstmt, rs, accountId, historyId, transactionAmount, BWPConstant.REJECTED);
        }
    }

    private static String preventSqlInjection(String SequenceName) {
        /*prevent sql injection*/
        SequenceName = SequenceName.replaceAll(" ", "");
        SequenceName = SequenceName.replaceAll(",", "");
        SequenceName = SequenceName.replaceAll(";", "");
        SequenceName = SequenceName.replaceAll("-", "");
        SequenceName = SequenceName.replaceAll("/", "");
        return SequenceName;
    }

    private int getSequence(Connection conn, PreparedStatement pstmt, ResultSet rs, String SequenceName) {
        int seq_id = -1;
        try {
            SequenceName = preventSqlInjection(SequenceName);
            pstmt = conn.prepareStatement("select NEXT VALUE FOR " + SequenceName + " as seq_id from SYSIBM.SYSDUMMY1");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                seq_id = rs.getInt("seq_id");
            }
        } catch (SQLException ex) {
        }
        return seq_id;
    }

    private int getAccoutId(Connection conn, PreparedStatement pstmt, ResultSet rs, String accountNumber) {
        try {
            int accountId = -1;
            pstmt = conn.prepareStatement("select account_id from " + BWPConstant.BALANCE_TABLE + " where account_number=?");
            pstmt.setString(1, accountNumber);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                accountId = rs.getInt("account_id");
            }
            return accountId;
        } catch (SQLException ex) {
        }
        return -1;
    }

    private void insertHistoryTable(Connection conn, PreparedStatement pstmt, ResultSet rs, int accountId, int historyId,
            Double transactionAmount, int transaction_status) {
        try {
            pstmt = conn.prepareStatement("insert into " + BWPConstant.HISTORY_TABLE
                    + "(history_id,account_id,transaction_amount,transaction_status,transaction_date,last_updated_date) values (?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)");
            pstmt.setInt(1, historyId);
            pstmt.setInt(2, accountId);
            pstmt.setDouble(3, transactionAmount);
            pstmt.setInt(4, transaction_status);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
        }
    }

    public int updateBalanceTbl(Connection conn, PreparedStatement pstmt, int accountId, Double transactionAmount) {
        try {
            pstmt = conn.prepareStatement("update " + BWPConstant.BALANCE_TABLE
                    + " b set b.last_updated_date=CURRENT_TIMESTAMP, b.balance_amount=(b.balance_amount+?) where (b.balance_amount+?)>=0 and b.account_id=?");
            pstmt.setDouble(1, transactionAmount);
            pstmt.setDouble(2, transactionAmount);
            pstmt.setInt(3, accountId);
            return pstmt.executeUpdate();
        } catch (SQLException ex) {
        }
        return -1;
    }
}
