/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blindwriteprotocol.consistency.init;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.derby.jdbc.ClientDriver;
import org.blindwriteprotocol.consistency.function.BWPConstant;

/**
 *
 * @author khairul.a
 */
public class CreateTableAndSequence {

    public static void main(String[] args) throws SQLException {
        init(Double.parseDouble("100"));
        //reset(Double.parseDouble("100"));
    }

    public static void init(Double initAmount) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        Driver d = new ClientDriver();
        String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
        try {
            conn = d.connect(url, null);
            dropHistoryIndex(conn, pstmt);
            dropTxnIndex(conn, pstmt);
            dropBalanceIndex(conn, pstmt);
            dropTable(conn, pstmt, BWPConstant.BALANCE_TABLE);
            dropTable(conn, pstmt, BWPConstant.HISTORY_TABLE);
            dropTable(conn, pstmt, BWPConstant.TRANSACTION_TABLE);

            createBalanceTable(conn, pstmt);
            createHistoryTable(conn, pstmt);
            createTxnTable(conn, pstmt);

            createBalanceIndex(conn, pstmt);
            createHistoryIndex(conn, pstmt);
            createTxnIndex(conn, pstmt);

            createSequence(conn, pstmt, BWPConstant.ACCOUNT_ID_SEQUENCE);

            insertBalanceTable(conn, pstmt, BWPConstant.FIRST_ACCOUNT_NUMBER, initAmount);
            insertBalanceTable(conn, pstmt, BWPConstant.SECOND_ACCOUNT_NUMBER, initAmount);
        } catch (SQLException ex) {
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    public static void createBalanceTable(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            pstmt = conn.prepareStatement("create table " + BWPConstant.BALANCE_TABLE + "(account_id integer, history_id integer, account_number varchar(50), balance_amount FLOAT, last_updated_date TIMESTAMP NOT NULL, CONSTRAINT balance_tbl_key PRIMARY KEY (account_id))");
            pstmt.executeUpdate();
            System.out.println("Table " + BWPConstant.BALANCE_TABLE + " created.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void createBalanceIndex(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            pstmt = conn.prepareStatement("create index balance_i1 on " + BWPConstant.BALANCE_TABLE + "(account_number)");
            pstmt.executeUpdate();
            System.out.println("Index balance_i1 created.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void dropBalanceIndex(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            pstmt = conn.prepareStatement("drop index balance_i1");
            pstmt.executeUpdate();
            System.out.println("Index balance_i1 dropped.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void dropTable(Connection conn, PreparedStatement pstmt, String tableName) throws SQLException {
        try {
            tableName = preventSqlInjection(tableName);
            pstmt = conn.prepareStatement("drop table " + tableName);
            pstmt.executeUpdate();
            System.out.println("Table " + tableName + " dropped.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void createHistoryTable(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            pstmt = conn.prepareStatement("create table " + BWPConstant.HISTORY_TABLE + "(history_id integer, prev_history_id integer,data_list_size integer,list_of_data varchar(10000),account_id integer, transaction_amount FLOAT, previous_balance_amount FLOAT, balance_amount FLOAT, transaction_date TIMESTAMP, last_updated_date TIMESTAMP, transaction_status integer)");
            pstmt.executeUpdate();
            System.out.println("Table " + BWPConstant.HISTORY_TABLE + " created.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void createTxnTable(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            pstmt = conn.prepareStatement("create table " + BWPConstant.TRANSACTION_TABLE + "(history_id integer,prev_history_id integer,data_list_size integer,list_of_data varchar(10000),account_id integer, transaction_amount FLOAT, previous_balance_amount FLOAT, balance_amount FLOAT, transaction_date TIMESTAMP, last_updated_date TIMESTAMP, transaction_status integer)");
            pstmt.executeUpdate();
            System.out.println("Table " + BWPConstant.TRANSACTION_TABLE + " created.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void createHistoryIndex(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            Statement stmt = conn.createStatement();
            String s = "create UNIQUE index history_i1 on " + BWPConstant.HISTORY_TABLE + "(account_id,history_id)";
            stmt.executeUpdate(s);
            System.out.println("Index history_i1 created.");
            s = "create index history_i2 on " + BWPConstant.HISTORY_TABLE + "(account_id)";
            stmt.executeUpdate(s);
            System.out.println("Index history_i2 created.");
            /*s = "create index history_i3 on " + BWPConstant.HISTORY_TABLE + "(transaction_status)";
            stmt.executeUpdate(s);
            System.out.println("Index history_i3 created.");
            s = "create index history_i4 on " + BWPConstant.HISTORY_TABLE + "(account_id,history_id,transaction_status)";
            stmt.executeUpdate(s);
            System.out.println("Index history_i4 created.");
            s = "create index history_i5 on " + BWPConstant.HISTORY_TABLE + "(account_id,transaction_status)";
            stmt.executeUpdate(s);
            System.out.println("Index history_i5 created.");*/
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void dropHistoryIndex(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            pstmt = conn.prepareStatement("drop index history_i1");
            pstmt.executeUpdate();
            System.out.println("Index history_i1 dropped.");
            pstmt = conn.prepareStatement("drop index history_i2");
            pstmt.executeUpdate();
            System.out.println("Index history_i2 dropped.");
            /*pstmt = conn.prepareStatement("drop index history_i3");
            pstmt.executeUpdate();
            System.out.println("Index history_i3 dropped.");
            pstmt = conn.prepareStatement("drop index history_i4");
            pstmt.executeUpdate();
            System.out.println("Index history_i4 dropped.");
            pstmt = conn.prepareStatement("drop index history_i5");
            pstmt.executeUpdate();
            System.out.println("Index history_i5 dropped.");*/
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void createTxnIndex(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            Statement stmt = conn.createStatement();
            String s = "create UNIQUE index txn_i1 on " + BWPConstant.TRANSACTION_TABLE + "(account_id,history_id)";
            stmt.executeUpdate(s);
            System.out.println("Index txn_i1 created.");
            s = "create index txn_i2 on " + BWPConstant.TRANSACTION_TABLE + "(account_id)";
            stmt.executeUpdate(s);
            System.out.println("Index txn_i2 created.");
            s = "create index txn_i3 on " + BWPConstant.TRANSACTION_TABLE + "(transaction_status)";
            stmt.executeUpdate(s);
            System.out.println("Index txn_i3 created.");
            s = "create index txn_i4 on " + BWPConstant.TRANSACTION_TABLE + "(account_id,history_id,transaction_status)";
            stmt.executeUpdate(s);
            System.out.println("Index txn_i4 created.");
            s = "create index txn_i5 on " + BWPConstant.TRANSACTION_TABLE + "(account_id,transaction_status)";
            stmt.executeUpdate(s);
            System.out.println("Index txn_i5 created.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static void dropTxnIndex(Connection conn, PreparedStatement pstmt) throws SQLException {
        try {
            pstmt = conn.prepareStatement("drop index txn_i1");
            pstmt.executeUpdate();
            System.out.println("Index txn_i1 dropped.");
            pstmt = conn.prepareStatement("drop index txn_i2");
            pstmt.executeUpdate();
            System.out.println("Index txn_i2 dropped.");
            pstmt = conn.prepareStatement("drop index txn_i3");
            pstmt.executeUpdate();
            System.out.println("Index txn_i3 dropped.");
            pstmt = conn.prepareStatement("drop index txn_i4");
            pstmt.executeUpdate();
            System.out.println("Index txn_i4 dropped.");
            pstmt = conn.prepareStatement("drop index txn_i5");
            pstmt.executeUpdate();
            System.out.println("Index txn_i5 dropped.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static String preventSqlInjection(String SequenceName) {
        /*prevent sql injection*/
        return SequenceName.replaceAll(" ", "").replaceAll(",", "").replaceAll(";", "").replaceAll("-", "").replaceAll("/", "");
    }

    public static void createSequence(Connection conn, PreparedStatement pstmt, String SequenceName) throws SQLException {
        try {
            dropSequence(conn, pstmt, SequenceName);
            SequenceName = preventSqlInjection(SequenceName);
            pstmt = conn.prepareStatement("CREATE SEQUENCE " + SequenceName + " AS BIGINT START WITH 1 INCREMENT BY 1 NO CYCLE");
            pstmt.executeUpdate();
            System.out.println("Sequence " + SequenceName + " created.");
        } catch (SQLException ex) {
            System.out.println("createSequence::ex.getSQLState()::" + ex.getSQLState() + "::ex.getMessage()::" + ex.getMessage());
        }
        conn.commit();
    }

    public static void dropSequence(Connection conn, PreparedStatement pstmt, String SequenceName) throws SQLException {
        try {
            SequenceName = preventSqlInjection(SequenceName);
            pstmt = conn.prepareStatement("drop SEQUENCE " + SequenceName + " RESTRICT");
            pstmt.executeUpdate();
            System.out.println("Sequence " + SequenceName + " dropped.");
        } catch (SQLException ex) {
        }
        conn.commit();
    }

    public static int getSequence(Connection conn, PreparedStatement pstmt, String SequenceName) throws SQLException {
        int seq_id = -1;
        try {
            SequenceName = preventSqlInjection(SequenceName);
            pstmt = conn.prepareStatement("select NEXT VALUE FOR " + SequenceName + " as seq_id from SYSIBM.SYSDUMMY1");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                seq_id = rs.getInt("seq_id");
            }
        } catch (SQLException ex) {
        }
        return seq_id;
    }

    public static void insertBalanceTable(Connection conn, PreparedStatement pstmt, String accountNumber, Double balanceAmount) throws SQLException {
        try {
            int accountId = getSequence(conn, pstmt, BWPConstant.ACCOUNT_ID_SEQUENCE);
            createSequence(conn, pstmt, BWPConstant.HISTORY_ID_SEQUENCE + accountNumber);
            int historyId = getSequence(conn, pstmt, BWPConstant.HISTORY_ID_SEQUENCE + accountNumber);
            pstmt = conn.prepareStatement("blind insert into " + BWPConstant.BALANCE_TABLE + "(account_id,account_number,balance_amount,history_id,last_updated_date) values (?,?,?,?,CURRENT_TIMESTAMP)");
            pstmt.setInt(1, accountId);
            pstmt.setString(2, accountNumber);
            pstmt.setDouble(3, balanceAmount);
            pstmt.setDouble(4, historyId);
            int howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record inserted into " + BWPConstant.BALANCE_TABLE + " by transaction 1 using blind insert.");
            insertTxnTable(conn, pstmt, accountId, historyId, balanceAmount);
        } catch (SQLException ex) {
        }
    }

    public static void insertTxnTable(Connection conn, PreparedStatement pstmt, int accountId, int historyId, Double transactionAmount) throws SQLException {
        try {
            pstmt = conn.prepareStatement("blind insert into " + BWPConstant.TRANSACTION_TABLE + "(history_id,account_id,transaction_amount,previous_balance_amount,balance_amount,transaction_status,transaction_date,last_updated_date) values (?,?,?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)");
            pstmt.setInt(1, historyId);
            pstmt.setInt(2, accountId);
            pstmt.setDouble(3, transactionAmount);
            pstmt.setDouble(4, Double.parseDouble("0"));
            pstmt.setDouble(5, transactionAmount);
            pstmt.setInt(6, BWPConstant.APPROVED);
            int howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record inserted into " + BWPConstant.TRANSACTION_TABLE + " by transaction 1 using blind insert.");
            insertHistoryTable(conn, pstmt, accountId, historyId, transactionAmount);
        } catch (SQLException ex) {
        }
    }

    public static void insertHistoryTable(Connection conn, PreparedStatement pstmt, int accountId, int historyId, Double transactionAmount) throws SQLException {
        try {
            pstmt = conn.prepareStatement("blind insert into " + BWPConstant.HISTORY_TABLE + "(history_id,account_id,transaction_amount,previous_balance_amount,balance_amount,transaction_status,transaction_date,last_updated_date) values (?,?,?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)");
            pstmt.setInt(1, historyId);
            pstmt.setInt(2, accountId);
            pstmt.setDouble(3, transactionAmount);
            pstmt.setDouble(4, Double.parseDouble("0"));
            pstmt.setDouble(5, transactionAmount);
            pstmt.setInt(6, BWPConstant.APPROVED);
            int howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record inserted into " + BWPConstant.HISTORY_TABLE + " by transaction 1 using blind insert.");
        } catch (SQLException ex) {
        }
    }

    public static void deleteTableHistory(Connection conn, PreparedStatement pstmt, String accountNumber) throws SQLException {
        try {
            pstmt = conn.prepareStatement("blind delete FROM " + BWPConstant.HISTORY_TABLE + " hh where hh.account_id=(select b.account_id from " + BWPConstant.BALANCE_TABLE + " b where b.account_number=?) and hh.history_id < (select max(h.history_id) from " + BWPConstant.HISTORY_TABLE + " h where h.account_id=hh.account_id)");
            pstmt.setString(1, accountNumber);
            int howmany = pstmt.executeUpdate();
            //System.out.println(howmany + " record deleted using blind delete.");
        } catch (SQLException ex) {
        }
    }

    public static void deleteTable(Connection conn, PreparedStatement pstmt, String tableName) throws SQLException {
        try {
            pstmt = conn.prepareStatement("blind delete FROM " + tableName);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
        }
    }

    public static void reset() throws SQLException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        Driver d = new ClientDriver();
        String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
        try {
            conn = d.connect(url, null);
            deleteTableHistory(conn, pstmt, BWPConstant.FIRST_ACCOUNT_NUMBER);
        } catch (SQLException ex) {
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void reset(Double initAmount) throws SQLException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        Driver d = new ClientDriver();
        String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
        try {
            conn = d.connect(url, null);
            deleteTable(conn, pstmt, BWPConstant.HISTORY_TABLE);
            deleteTable(conn, pstmt, BWPConstant.BALANCE_TABLE);
            insertBalanceTable(conn, pstmt, BWPConstant.FIRST_ACCOUNT_NUMBER, initAmount);
            insertBalanceTable(conn, pstmt, BWPConstant.SECOND_ACCOUNT_NUMBER, initAmount);
        } catch (SQLException ex) {

        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}
