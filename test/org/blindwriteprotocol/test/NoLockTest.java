package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class NoLockTest extends TestCase {

    private Connection conn1 = null;
    private Connection conn2 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                String s = "create table test_tbl(t_id int, t_text varchar(100))";
                stmt.executeUpdate(s);
                System.out.println("Table test_tbl created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("drop table test_tbl");
                System.out.println("Table test_tbl dropped.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    public void testNoLockTest() throws SQLException {
        Statement stmt1 = null;
        Statement stmt2 = null;
        ResultSet rs = null;
        boolean hasError = false;
        try {
            conn1 = getConnection();
            conn2 = getConnection();
            stmt1 = conn1.createStatement();
            int howmany = stmt1.executeUpdate("blind insert into test_tbl(t_id, t_text) "
                    + "values (1, 'blind write protocol')");
            System.out.println(howmany + " records inserted by transaction 1 using blind insert.");
            stmt2 = conn2.createStatement();
            System.out.println("Before select:" + new Date().toString());
            rs = stmt2.executeQuery("select * from test_tbl");
            int i = 0;
            while (rs.next()) {
                i++;
            }
            System.out.println(i + " records selected by transaction 2.");
            assertTrue(i == 1);
            System.out.println("After select:" + new Date().toString());
        } catch (SQLException se) {
            hasError = true;
        } finally {
            //stmt1.close();
            //stmt2.close();
            //conn1.rollback();
            //conn2.rollback();
            conn1.close();
            conn2.close();
        }
        assertFalse(hasError);
    }

}
