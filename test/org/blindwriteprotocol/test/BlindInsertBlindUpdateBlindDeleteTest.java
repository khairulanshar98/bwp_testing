package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class BlindInsertBlindUpdateBlindDeleteTest extends TestCase {

    private Connection conn1 = null;
    private Connection conn2 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                String s = "create table test_tbl(t_id int, t_text varchar(100))";
                stmt.executeUpdate(s);
                System.out.println("Table test_tbl created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("drop table test_tbl");
                System.out.println("Table test_tbl dropped.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    public void testBlindInsertBlindUpdateBlindDeleteTest() throws SQLException {
        PreparedStatement pstmt = null;
        Statement stmt2 = null;
        ResultSet rs = null;
        Date startSqlError = null;
        try {
            conn1 = getConnection();
            conn2 = getConnection();

            pstmt = conn1.prepareStatement("blind insert into test_tbl(t_id, t_text) values (?, ?)");
            pstmt.setInt(1, 1);
            pstmt.setString(2, "blind insert protocol");
            int howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record inserted by transaction 1 using blind insert.");
            
            stmt2 = conn2.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            String t_text = "";
            int t_id = -1;
            int i = 0;
            while (rs.next()) {
                t_text = rs.getString("t_text");
                t_id = rs.getInt("t_id");
                i++;
            }
            System.out.println(i + " records selected by transaction 2.");
            System.out.println("t_id ==> " + t_id);
            System.out.println("t_text ==> " + t_text);
            assertTrue(i == 1);
            assertEquals(t_id, 1);
            assertEquals(t_text, "blind insert protocol");

            pstmt = conn2.prepareStatement("blind update test_tbl set t_id=?, t_text=? where t_id=?");
            pstmt.setInt(1, 2);
            pstmt.setString(2, "blind update protocol");
            pstmt.setInt(3, 1);
            howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record updated by transaction 2 using blind update.");
            
            stmt2 = conn1.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            t_text = "";
            t_id = -1;
            i = 0;
            while (rs.next()) {
                t_text = rs.getString("t_text");
                t_id = rs.getInt("t_id");
                i++;
            }
            System.out.println(i + " records selected by transaction 1.");
            System.out.println("t_id ==> " + t_id);
            System.out.println("t_text ==> " + t_text);
            assertTrue(i == 1);
            assertEquals(t_id, 2);
            assertEquals(t_text, "blind update protocol");

            pstmt = conn1.prepareStatement("blind delete from test_tbl where t_id=?");
            pstmt.setInt(1, 2);
            howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record deleted by transaction 1 using blind delete.");
            stmt2 = conn2.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            i = 0;
            while (rs.next()) {
                i++;
            }
            System.out.println(i + " records selected by transaction 2.");
            assertTrue(i == 0);

        } catch (SQLException se) {

        } finally {
            //stmt1.executeUpdate("drop table test_tbl");
            conn1.commit();
            conn2.commit();
            conn1.close();
            conn2.close();
        }
    }

}
