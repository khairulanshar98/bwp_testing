package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class SequenceTest extends TestCase {

    private Connection conn1 = null;
    private Connection conn2 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                String s = "create table test_tbl(t_id INTEGER, t_text varchar(100))";
                stmt.executeUpdate(s);
                System.out.println("Table test_tbl created.");
                stmt.executeUpdate("CREATE SEQUENCE t_id_seq AS INTEGER START WITH 1 INCREMENT BY 1");
                System.out.println("Sequence t_id_seq created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("drop table test_tbl");
                System.out.println("Table test_tbl dropped.");
                stmt.executeUpdate("drop SEQUENCE t_id_seq RESTRICT");
                System.out.println("Sequence t_id_seq dropped.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    public void testAutoIncrementTest() throws SQLException {
        PreparedStatement pstmt = null;
        Statement stmt1 = null;
        Statement stmt2 = null;
        ResultSet rs = null;
        Date startSqlError = null;
        try {
            conn1 = getConnection();
            conn2 = getConnection();

            pstmt = conn1.prepareStatement("blind insert into test_tbl(t_id,t_text) values (NEXT VALUE FOR t_id_seq,?)");
            pstmt.setString(1, "blind write protocol");
            int howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record inserted by transaction 1 using blind insert.");
            stmt2 = conn2.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            int t_id = -1;
            int i = 0;
            while (rs.next()) {
                t_id = rs.getInt("t_id");
                i++;
            }
            System.out.println(i + " record selected by transaction 2.");
            System.out.println("t_id ==> " + t_id);
            assertTrue(i == 1);
            assertEquals(t_id, 1);

            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (stmt2 != null) {
                stmt2.close();
            }

            pstmt = conn1.prepareStatement("blind insert into test_tbl(t_id,t_text) values (NEXT VALUE FOR t_id_seq,?)");
            pstmt.setString(1, "blind write protocol");
            howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record inserted by transaction 1 using blind insert.");
            stmt2 = conn2.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            i = 0;
            while (rs.next()) {
                t_id = rs.getInt("t_id");
                i++;
            }
            System.out.println(i + " record selected by transaction 2.");
            System.out.println("t_id ==> " + t_id);
            assertTrue(i == 2);
            assertEquals(t_id, 2);
            
            
            rs = stmt2.executeQuery("select NEXT VALUE FOR t_id_seq as t_id from SYSIBM.SYSDUMMY1");
            i = 0;
            while (rs.next()) {
                t_id = rs.getInt("t_id");
                i++;
            }
            System.out.println(i + " record selected by transaction 2.");
            System.out.println("t_id ==> " + t_id);
            assertTrue(i == 1);
            assertEquals(t_id, 3);
            
            stmt1 = conn1.createStatement();
            rs = stmt1.executeQuery("select NEXT VALUE FOR t_id_seq as t_id from SYSIBM.SYSDUMMY1");
            i = 0;
            while (rs.next()) {
                t_id = rs.getInt("t_id");
                i++;
            }
            System.out.println(i + " record selected by transaction 1.");
            System.out.println("t_id ==> " + t_id);
            assertTrue(i == 1);
            assertEquals(t_id, 4);

        } catch (SQLException se) {

        } finally {
            //stmt1.executeUpdate("drop table test_tbl");
            conn1.commit();
            conn2.commit();
            conn1.close();
            conn2.close();
        }
    }

}
