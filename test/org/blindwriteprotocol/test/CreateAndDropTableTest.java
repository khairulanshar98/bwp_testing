package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class CreateAndDropTableTest extends TestCase {

    private Connection conn1 = null;
    private Connection conn2 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("create table test_tbl(t_id int, t_text varchar(100))");
                System.out.println("Table test_tbl created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("drop table test_tbl");
                System.out.println("Table test_tbl dropped.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    public void testCreateAndDropTableTest() throws SQLException {
        Statement stmt1 = null;
        Statement stmt2 = null;
        ResultSet rs = null;
        Date startInsert = null;
        Date startSqlError = null;
        try {
            conn1 = getConnection();
            conn2 = getConnection();
            stmt1 = conn1.createStatement();
            startInsert = new Date();
            int howmany = stmt1.executeUpdate("blind insert into test_tbl(t_id, t_text) values (1, 'blind write protocol')");
            System.out.println(howmany + " record inserted by transaction 1 using blind insert.");
            stmt2 = conn2.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            int i = 0;
            while (rs.next()) {
                i++;
            }
            System.out.println(i + " record selected by transaction 2.");
            assertTrue(i == 1);
            
        } catch (SQLException se) {

        } finally {
            conn1.commit();
            conn2.commit();
            conn1.close();
            conn2.close();
        }
    }

}
