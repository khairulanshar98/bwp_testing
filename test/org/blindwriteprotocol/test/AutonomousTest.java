package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class AutonomousTest extends TestCase {

    private Connection conn1 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                String s = "create table test_tbl(t_id int, t_text varchar(100))";
                stmt.executeUpdate(s);
                System.out.println("Table test_tbl created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("drop table test_tbl");
                System.out.println("Table test_tbl dropped.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    public void testAutonomousTest() throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean hasError = false;
        try {
            conn1 = getConnection();
            pstmt = conn1.prepareStatement("insert into test_tbl(t_id, t_text) values (?,?)");
            pstmt.setInt(1, 1);
            pstmt.setString(2, "normal write protocol");
            int howmany = pstmt.executeUpdate();
            System.out.println(howmany + " records inserted by transaction 1 using normal insert.");
            
            pstmt = conn1.prepareStatement("blind insert into test_tbl(t_id, t_text) values (?,?)");
            pstmt.setInt(1, 2);
            pstmt.setString(2, "blind write protocol");
            howmany = pstmt.executeUpdate();
            System.out.println(howmany + " records inserted by transaction 1 using blind insert.");
            
            pstmt = conn1.prepareStatement("select * from test_tbl");
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) { i++; }
            System.out.println(i + " records selected by transaction 1 before rollback.");
            assertTrue(i == 2);
            
            conn1.rollback();
            System.out.println("Perform rollback.");
            
            pstmt = conn1.prepareStatement("select * from test_tbl");
            rs = pstmt.executeQuery();
            i = 0;
            while (rs.next()) { i++; }
            System.out.println(i + " records selected by transaction 1 after rollback.");
            assertTrue(i == 1);
        } catch (SQLException se) {
            hasError = true;
        } finally {
            conn1.rollback();
            conn1.close();
        }
        assertFalse(hasError);
    }

}
