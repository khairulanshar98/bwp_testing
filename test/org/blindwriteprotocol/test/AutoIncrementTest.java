package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class AutoIncrementTest extends TestCase {

    private Connection conn1 = null;
    private Connection conn2 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                String s = "create table test_tbl(t_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), t_text varchar(100),CONSTRAINT test_tbl_key PRIMARY KEY (t_id))";
                stmt.executeUpdate(s);
                System.out.println("Table test_tbl created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("drop table test_tbl");
                System.out.println("Table test_tbl dropped.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    public void testAutoIncrementTest() throws SQLException {
        PreparedStatement pstmt = null;
        Statement stmt1 = null;
        Statement stmt2 = null;
        ResultSet rs = null;
        Date startSqlError = null;
        try {
            conn1 = getConnection();
            conn2 = getConnection();

            pstmt = conn1.prepareStatement("blind insert into test_tbl(t_text) values (?)");
            pstmt.setString(1, "blind write protocol");
            int howmany = pstmt.executeUpdate();
            System.out.println(howmany + " record inserted by transaction 1 using blind insert.");
            stmt2 = conn2.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            int t_id = -1;
            int i = 0;
            while (rs.next()) {
                t_id = rs.getInt("t_id");
                i++;
            }
            System.out.println(i + " record selected by transaction 2.");
            System.out.println("t_id ==> " + t_id);
            assertTrue(i == 1);
            assertEquals(t_id, 1);

            
             
            pstmt = conn1.prepareStatement("blind INSERT INTO test_tbl (t_text) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, "blind write protocol");
            i = pstmt.executeUpdate();
            System.out.println(i + " record inserted by transaction 1.");
            rs=pstmt.getGeneratedKeys();
            //pstmt = conn1.prepareStatement("select IDENTITY_VAL_LOCAL() from SYSIBM.SYSDUMMY1");
            //rs = pstmt.executeQuery();
            i = 0;
            while (rs.next()) {
                t_id = rs.getInt(1);
                i++;
            }
            System.out.println("t_id getGeneratedKeys ==> " + t_id);
            if (rs != null) {
                rs.close();
            }
            if (stmt1 != null) {
                stmt1.close();
            }

            stmt2 = conn2.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            i = 0;
            while (rs.next()) {
                t_id = rs.getInt("t_id");
                i++;
            }
            System.out.println(i + " record selected by transaction 2.");
            System.out.println("t_id ==> " + t_id);
            assertTrue(i == 2);
            assertEquals(t_id, 2);
        } catch (SQLException se) {

        } finally {
            //stmt1.executeUpdate("drop table test_tbl");
            conn1.commit();
            conn2.commit();
            conn1.close();
            conn2.close();
        }
    }

}
