package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class PersistentTest extends TestCase {

    private Connection conn1 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                String s = "create table test_tbl(t_id int, t_text varchar(100))";
                stmt.executeUpdate(s);
                System.out.println("Table test_tbl created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        System.out.println("Please restart the Apache Derby database and perform select statment operation using ij tool.");
    }

    public void testPersistentTest() throws SQLException {
        Statement stmt1 = null;
        ResultSet rs = null;
        boolean hasError = false;
        try {
            conn1 = getConnection();
            stmt1 = conn1.createStatement();
            int howmany = stmt1.executeUpdate("insert into test_tbl(t_id, t_text) "
                    + "values (1, 'normal write protocol')");
            System.out.println(howmany + " records inserted by transaction 1 using normal insert.");
            stmt1 = conn1.createStatement();
            howmany = stmt1.executeUpdate("blind insert into test_tbl(t_id, t_text) "
                    + "values (2, 'blind write protocol')");
            System.out.println(howmany + " records inserted by transaction 1 using blind insert.");
            System.out.println("Before select:" + new Date().toString());
            stmt1 = conn1.createStatement();
            rs = stmt1.executeQuery("select * from test_tbl");
            int i = 0;
            while (rs.next()) { i++; }
            System.out.println(i + " records selected by transaction 1 before rollback.");
            assertTrue(i == 2);
            conn1.rollback();
            stmt1 = conn1.createStatement();
            rs = stmt1.executeQuery("select * from test_tbl");
            i = 0;
            while (rs.next()) { i++; }
            System.out.println(i + " records selected by transaction 1 after rollback.");
            assertTrue(i == 1);
        } catch (SQLException se) {
            hasError = true;
        } finally {
            stmt1.close();
            conn1.rollback();
            conn1.close();
        }
        assertFalse(hasError);
    }

}
