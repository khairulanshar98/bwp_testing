package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class NoWaitTest extends TestCase {

    private Connection conn1 = null;
    private Connection conn2 = null;
    private Connection conn3 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                String s = "create table test_tbl(t_id int, t_text varchar(100))";
                stmt.executeUpdate(s);
                System.out.println("Table test_tbl created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("drop table test_tbl");
                System.out.println("Table test_tbl dropped.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    public void testNoLockTest() throws SQLException {
        Statement stmt1 = null;
        Statement stmt2 = null;
        Statement stmt3 = null;
        ResultSet rs = null;
        boolean hasError = false;
        try {
            conn1 = getConnection();
            conn2 = getConnection();
            conn3 = getConnection();
            
            stmt1 = conn1.createStatement();
            int howmany = stmt1.executeUpdate("insert into test_tbl(t_id, t_text) "
                    + "values (1, 'normal write protocol')");
            System.out.println(howmany + " records inserted by transaction 1 using normal insert.");
            
            stmt2 = conn2.createStatement();
            howmany = stmt2.executeUpdate("blind insert into test_tbl(t_id, t_text) "
                    + "values (2, 'blind write protocol')");
            System.out.println(howmany + " records inserted by transaction 1 using blind insert.");
            
            
            stmt3 = conn3.createStatement();
            howmany = stmt2.executeUpdate("insert into test_tbl(t_id, t_text) "
                    + "values (3, 'normal write protocol')");
            System.out.println(howmany + " records inserted by transaction 1 using normal insert.");
        } catch (SQLException se) {
            hasError = true;
            System.out.println("se.getSQLState():" + se.getSQLState() + "::" + se.getMessage());
        } finally {
            stmt1.close();
            stmt2.close();
            conn1.rollback();
            conn2.rollback();
            conn1.close();
            conn2.close();
        }
        assertFalse(hasError);
    }

}
