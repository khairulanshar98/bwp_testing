package org.blindwriteprotocol.test;


import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import org.apache.derby.jdbc.ClientDriver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class ApacheDerbyConnection {

    private Connection conn = null;

    public ApacheDerbyConnection() throws SQLException {
        Driver d = new ClientDriver();
        String url = "jdbc:derby://localhost:1528/bwp_testing;create=true;user=app;password=app";
        conn = d.connect(url, null);
    }

    public Connection getConnection() {
        return conn;
    }

    public static void main(String[] args) throws SQLException {
        Connection conn1 = new ApacheDerbyConnection().getConnection();
        System.out.println("" + conn1.getAutoCommit());
    }

}
