package org.blindwriteprotocol.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import junit.framework.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khairul.a
 */
public class CacheTest extends TestCase {

    private Connection conn1 = null;
    private Connection conn2 = null;

    public Connection getConnection() throws SQLException {
        return new ApacheDerbyConnection().getConnection();
    }

    @Override
    protected void setUp() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                String s = "create table test_tbl(t_id int, t_text varchar(100))";
                stmt.executeUpdate(s);
                System.out.println("Table test_tbl created.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    @Override
    protected void tearDown() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            try (Statement stmt = conn.createStatement()) {
                stmt.executeUpdate("drop table test_tbl");
                System.out.println("Table test_tbl dropped.");
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } finally {
            conn.commit();
            conn.close();
        }
    }

    public void testCacheTest() throws SQLException {
        Statement stmt1 = null;
        Statement stmt2 = null;
        ResultSet rs = null;
        boolean hasError = false;
        try {
            conn1 = getConnection();
            conn2 = getConnection();
            stmt1 = conn1.createStatement();
            int howmany = stmt1.executeUpdate("blind insert into test_tbl(t_id, t_text) "
                    + "values (2, 'blind write protocol')");
            System.out.println(howmany + " records inserted by transaction 1 using blind insert.");
            stmt1 = conn1.createStatement();
            howmany = stmt1.executeUpdate("blind insert into test_tbl(t_id, t_text) "
                    + "values (2, 'blind write protocol')");
            System.out.println(howmany + " records inserted by transaction 1 using blind insert parsing from the cache.");
            conn1.rollback();

            stmt2 = conn2.createStatement();
            rs = stmt2.executeQuery("select * from test_tbl");
            int i = 0;
            while (rs.next()) {
                i++;
            }
            System.out.println(i + " records selected by transaction 1.");
            assertTrue(i == 2);
        } catch (SQLException se) {
            hasError = true;
        } finally {
            conn2.rollback();
            conn1.close();
            conn2.close();
        }
        assertFalse(hasError);
    }

}
